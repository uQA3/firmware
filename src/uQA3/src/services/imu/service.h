#ifndef SERVICE_IMU_IN_H
#define SERVICE_IMU_IN_H
#include <zephyr.h>
#include <sensor.h>
#include <stdio.h>
#include <logging/sys_log.h>
#include "../service.h"

extern config_service_t service_imu;

#endif
