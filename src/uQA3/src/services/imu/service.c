#include "service.h"

#define DECIMATION_FACTOR 100

K_SEM_DEFINE(sem, 0, 1);

static void trigger_handler(struct device *dev, struct sensor_trigger *trigger)
{
	ARG_UNUSED(trigger);

	if (sensor_sample_fetch(dev)) {
		SYS_LOG_DBG("sensor_sample_fetch failed");
		return;
	}

	k_sem_give(&sem);
}

void imu_thread(void)
{
	struct sensor_value accel[3];
	struct sensor_value gyro[3];
	struct sensor_value temp;

	struct device *dev = device_get_binding(CONFIG_MPU6050_NAME);

	if (dev == NULL) {
		SYS_LOG_ERR("Could not get tu vieja device\n");
		return;
	}

	struct sensor_trigger trig = {
		.type = SENSOR_TRIG_DATA_READY,
		.chan = SENSOR_CHAN_ACCEL_XYZ,
	};

    if (sensor_trigger_set(dev, &trig, trigger_handler)) {
		SYS_LOG_ERR("Could not set trigger\n");
		return;
	}


	while (1) {
		k_sem_take(&sem, K_FOREVER);

		sensor_channel_get(dev, SENSOR_CHAN_ACCEL_XYZ, accel);
		sensor_channel_get(dev, SENSOR_CHAN_GYRO_XYZ, gyro);
		sensor_channel_get(dev, SENSOR_CHAN_TEMP, &temp);

	    k_busy_wait(USEC_PER_MSEC*1000);

		/* Print accel x,y,z and mag x,y,z data */
		printf("AX=%10.6f AY=%10.6f AZ=%10.6f "
		       "GX=%10.6f GY=%10.6f GZ=%10.6f "
		       "T=%10.6f\n",
		       sensor_value_to_double(&accel[0]),
		       sensor_value_to_double(&accel[1]),
		       sensor_value_to_double(&accel[2]),
		       sensor_value_to_double(&gyro[0]),
		       sensor_value_to_double(&gyro[1]),
		       sensor_value_to_double(&gyro[2]),
		       sensor_value_to_double(&temp));
    }
}
 
config_service_t service_imu = {
    .name = "Imu Reader",
    .entrypoint = imu_thread,
    .priority = 3
};
