#include "service.h"

#define SLEEPTIME 500

void helloLoop(const char *my_name)
{
	while (1) {
		printk("%s: Hello World from %s!\n", my_name, CONFIG_ARCH);

		k_sleep(SLEEPTIME);
	}
}

void threadA(void *dummy1, void *dummy2, void *dummy3)
{
	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);
    
	helloLoop(__func__);
}

 
config_service_t service_echo = {
    .name = "A",
    .entrypoint = threadA,
    .priority = 0,
};
