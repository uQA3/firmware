#ifndef ECHO_SERVICE_H
#define ECHO_SERVICE_H

#include <zephyr.h>
#include "../service.h"
#include <misc/printk.h>

extern config_service_t service_echo;

#endif
