#include "service.h"

void start_services(config_service_t *const *const services, int count){
    int i= 0;
    for(i=0; i<count; i++){
	    k_thread_create(&services[i]->thread, &services[i]->thread_stack_area, 1024, services[i]->entrypoint, NULL, NULL, NULL,
	    	       services[i]->priority, 0, K_NO_WAIT);
    };

};
