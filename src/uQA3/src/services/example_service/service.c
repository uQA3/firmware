#include "service.h"

#define SLEEPTIME 500

void exampleLoop(const char *my_name)
{
	while (1) {
		printk("%s: Hello World from %s!\n", my_name, CONFIG_ARCH);

		k_sleep(SLEEPTIME);
	}
}

void exampleThread(void *dummy1, void *dummy2, void *dummy3)
{
	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);
    
	exampleLoop(__func__);
}

 
config_service_t service_example = {
    .name = "Example Service",
    .entrypoint = exampleThread,
    .priority = 0
};
