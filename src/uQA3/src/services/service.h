#ifndef SERVICE_H
#define SERVICE_H

#include <zephyr.h>

#define STACKSIZE 1024

typedef struct _service_t {
    char *const name;
    int stacksize;
    void * entrypoint;
    uint8_t priority;
    char thread_stack_area[STACKSIZE];
    struct k_thread thread;
} config_service_t;

void start_services(config_service_t *const *const services, int count);

#endif 
