#include <zephyr.h>
#include <misc/printk.h>
#include "services/service.h"
#include "services/echo/service.h"
#include "services/imu/service.h"

#define STACKSIZE 1024
#define PRIORITY 7
#define SLEEPTIME 500

static config_service_t *services_list[] = {
    &service_echo,
    &service_imu,
};

void threadMain(void *dummy1, void *dummy2, void *dummy3)
{
	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);
    
    start_services(services_list, 2);
}

K_THREAD_DEFINE(threadMain_id, STACKSIZE, threadMain, NULL, NULL, NULL,
		PRIORITY, 0, K_NO_WAIT);
