## How to Hello World
Plug the uQA3 to your computer and run the dockershell `./dockershell`

This environment has all the necessary tools installed to build without installing packages on your PC.

1/ Create `build` dir in the app you want to build.
```
cd /root/zephyr/samples/hello_world 
mkdir build && cd build
```
2/ Run cmake configure
```
cmake -GNinja -DBOARD=esp32 ..
```
3/ Build and flash
```
ninja flash
```

This folder is mounted as a volume in `/root/workdir`



## How to build uQA3

Inside the dockershell, run:

```
cd /root/workdir/src/uQA3
mkdir build && cd build
cmake -GNinja -DBOARD=esp32 ..
ninja flash
```
