FROM debian:unstable

#RUN echo 'Acquire::HTTP::Proxy "http://192.168.0.240:3142";' >> /etc/apt/apt.conf.d/01proxy \ 
#    && echo 'Acquire::HTTPS::Proxy "false";' >> /etc/apt/apt.conf.d/01proxy;

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    wget \
    eatmydata

RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    python3 \
    python3-pip \
    python3-setuptools \
    python3-wheel

# Python2 dependencies for esp-idf
RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    python \
    python-pip \
    python-setuptools \
    python-wheel

WORKDIR /root/esp

# Install espressif
RUN wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    tar -xzf xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    rm xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz

# install idf + py2+3 deps
RUN eatmydata git clone --recursive https://github.com/espressif/esp-idf.git && \
    eatmydata python3 -m pip install -r esp-idf/requirements.txt && \
    eatmydata python -m pip install -r esp-idf/requirements.txt

WORKDIR /root

# Clone zephyr
RUN eatmydata git clone https://github.com/zephyrproject-rtos/zephyr

# Install compiling dependencies
RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    device-tree-compiler \
    python3-pyelftools \
    cmake \
    gperf 

# Ninja to flash
RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    ninja-build \
    python \
    python-serial

# Developing tools
RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    vim \
    ctags \
    tmux

ENV ZEPHYR_BASE "/root/zephyr"
ENV ZEPHYR_TOOLCHAIN_VARIANT "espressif"
ENV ESP_IDF_PATH "/root/esp/esp-idf"
ENV ESPRESSIF_TOOLCHAIN_PATH "/root/esp/xtensa-esp32-elf/"

RUN cd $ZEPHYR_BASE && ctags -R . && cd -


# Dependencies for building uQA3/firmware_esp32
RUN eatmydata apt-get install -y --no-install-recommends \
    gcc \
    make \
    libncurses-dev \
    flex \
    bison

ENV PATH "${PATH}:/root/esp/xtensa-esp32-elf/bin"
ENV IDF_PATH /root/esp/esp-idf

RUN git clone https://github.com/kylehendricks/esp32-vl53l0x $IDF_PATH/components/esp32-vl53l0x
COPY vl53l0x-cmake $IDF_PATH/components/esp32-vl53l0x/CMakeLists.txt
# END Dependencies for building uQA3/firmware_esp32

WORKDIR /root/workdir
